<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231109110924 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category CHANGE display_order display_order INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_history ADD order_date DATETIME DEFAULT NULL, ADD shipping_amount DOUBLE PRECISION DEFAULT NULL, ADD discount_amount DOUBLE PRECISION DEFAULT NULL, CHANGE order_discount order_discount DOUBLE PRECISION DEFAULT NULL, CHANGE tax_amount tax_amount DOUBLE PRECISION DEFAULT NULL, CHANGE subtotal subtotal DOUBLE PRECISION DEFAULT NULL, CHANGE payment_date tracking_number VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_item DROP FOREIGN KEY FK_52EA1F09A76ED395');
        $this->addSql('DROP INDEX IDX_52EA1F09A76ED395 ON order_item');
        $this->addSql('ALTER TABLE order_item ADD payment_date DATETIME DEFAULT NULL, DROP user_id, CHANGE quantity quantity INT DEFAULT NULL, CHANGE price_per_unit price_per_unit DOUBLE PRECISION DEFAULT NULL, CHANGE total_price total_price DOUBLE PRECISION DEFAULT NULL, CHANGE discount_amount discount_amount DOUBLE PRECISION DEFAULT NULL, CHANGE tax_amount tax_amount DOUBLE PRECISION DEFAULT NULL, CHANGE subtotal subtotal DOUBLE PRECISION DEFAULT NULL, CHANGE gift_message gift_message LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE orderr CHANGE total_amount total_amount DOUBLE PRECISION DEFAULT NULL, CHANGE tax_amount tax_amount DOUBLE PRECISION DEFAULT NULL, CHANGE shipping_amount shipping_amount DOUBLE PRECISION DEFAULT NULL, CHANGE subtotal subtotal DOUBLE PRECISION DEFAULT NULL, CHANGE discount_amount discount_amount DOUBLE PRECISION DEFAULT NULL, CHANGE delivery_date delivery_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE price price DOUBLE PRECISION DEFAULT NULL, CHANGE discount_price discount_price DOUBLE PRECISION DEFAULT NULL, CHANGE stock_quantity stock_quantity DOUBLE PRECISION DEFAULT NULL, CHANGE number_of_ratings number_of_ratings INT DEFAULT NULL, CHANGE weight weight DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category CHANGE display_order display_order VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_history DROP order_date, DROP shipping_amount, DROP discount_amount, CHANGE order_discount order_discount VARCHAR(255) DEFAULT NULL, CHANGE tax_amount tax_amount VARCHAR(255) DEFAULT NULL, CHANGE subtotal subtotal VARCHAR(255) DEFAULT NULL, CHANGE tracking_number payment_date VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_item ADD user_id INT DEFAULT NULL, DROP payment_date, CHANGE quantity quantity VARCHAR(255) DEFAULT NULL, CHANGE price_per_unit price_per_unit VARCHAR(255) DEFAULT NULL, CHANGE total_price total_price VARCHAR(255) DEFAULT NULL, CHANGE discount_amount discount_amount VARCHAR(255) DEFAULT NULL, CHANGE tax_amount tax_amount VARCHAR(255) DEFAULT NULL, CHANGE subtotal subtotal VARCHAR(255) DEFAULT NULL, CHANGE gift_message gift_message VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_52EA1F09A76ED395 ON order_item (user_id)');
        $this->addSql('ALTER TABLE orderr CHANGE total_amount total_amount VARCHAR(255) DEFAULT NULL, CHANGE tax_amount tax_amount VARCHAR(255) DEFAULT NULL, CHANGE shipping_amount shipping_amount VARCHAR(255) DEFAULT NULL, CHANGE subtotal subtotal VARCHAR(255) DEFAULT NULL, CHANGE discount_amount discount_amount VARCHAR(255) DEFAULT NULL, CHANGE delivery_date delivery_date VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE discount_price discount_price VARCHAR(255) DEFAULT NULL, CHANGE stock_quantity stock_quantity VARCHAR(255) DEFAULT NULL, CHANGE number_of_ratings number_of_ratings VARCHAR(255) DEFAULT NULL, CHANGE weight weight VARCHAR(255) DEFAULT NULL');
    }
}
