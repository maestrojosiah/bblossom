<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231228150951 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cart_merge DROP FOREIGN KEY FK_B483FA51A76ED395');
        $this->addSql('DROP INDEX IDX_B483FA51A76ED395 ON cart_merge');
        $this->addSql('ALTER TABLE cart_merge ADD email VARCHAR(180) NOT NULL, DROP user_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cart_merge ADD user_id INT DEFAULT NULL, DROP email');
        $this->addSql('ALTER TABLE cart_merge ADD CONSTRAINT FK_B483FA51A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_B483FA51A76ED395 ON cart_merge (user_id)');
    }
}
