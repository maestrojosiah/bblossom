<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Account
    |--------------------------------------------------------------------------
    |
    | This is the default account to be used when none is specified.
    */

    'default' => 'production',

    /*
    |--------------------------------------------------------------------------
    | Native File Cache Location
    |--------------------------------------------------------------------------
    |
    | When using the Native Cache driver, this will be the relative directory
    | where the cache information will be stored.
    */

    'cache_location' => '../cache',

    /*
    |--------------------------------------------------------------------------
    | Accounts
    |--------------------------------------------------------------------------
    |
    | These are the accounts that can be used with the package. You can configure
    | as many as needed. Two have been setup for you.
    |
    | Sandbox: Determines whether to use the sandbox, Possible values: sandbox | production
    | Initiator: This is the username used to authenticate the transaction request
    | LNMO:
    |    paybill: Your paybill number
    |    shortcode: Your business shortcode
    |    passkey: The passkey for the paybill number
    |    callback: Endpoint that will be be queried on completion or failure of the transaction.
    |
    */

    'accounts' => [
        'staging' => [
            'sandbox' => true,
            'key' => 'dDmAO2n5lYjRAKC8v44UQiWvb5miSK9faJOIvnqaik17HZhI',
            'secret' => '8OwaG1oHMKMwFfTg5mZ1GvAy5VGl6OSjL5MAfG2dGYDGG1gECCShGlqVdtbIMfaZ',
            'initiator' => 'apitest363',
            'id_validation_callback' => 'https://babyblossom.co.ke/cb/bb/mps',
            'lnmo' => [
                'paybill' => 174379,
                'shortcode' => 174379,
                'passkey' => 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919',
                'callback' => 'https://babyblossom.co.ke/cb/bb/mps',
            ]
        ],

        'production' => [
            'sandbox' => false,
            'key' => 'LSxU0HGAYZxddDC9qqr0A3bBLB0SAJPREJf5we9H1gxZwegm',
            'secret' => 'DjaeucPfkCtFeuWVAgYrLG5PtqFTLWBT3pT8AEqsePnatB02TTxUIbv52OmXcx9i',
            'initiator' => 'BLOSSOM BABY CO LIMITED',
            'id_validation_callback' => 'https://babyblossom.co.ke/cb/bb/mps',
            'lnmo' => [
                'paybill' => 5329541,
                'shortcode' => 7335073,
                'passkey' => '2236f32e75ada1053e43f5056a4e91966bed0c7e5c258347fb12d189f1a388db',
                'callback' => 'https://babyblossom.co.ke/cb/bb/mps',
            ]
        ],
    ],
];
