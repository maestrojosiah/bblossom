<?php

namespace App\Repository;

use App\Entity\ProductDisplay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductDisplay>
 *
 * @method ProductDisplay|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductDisplay|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductDisplay[]    findAll()
 * @method ProductDisplay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductDisplayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductDisplay::class);
    }

//    /**
//     * @return ProductDisplay[] Returns an array of ProductDisplay objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ProductDisplay
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
