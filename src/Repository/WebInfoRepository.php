<?php

namespace App\Repository;

use App\Entity\WebInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<WebInfo>
 *
 * @method WebInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method WebInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method WebInfo[]    findAll()
 * @method WebInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebInfo::class);
    }

//    /**
//     * @return WebInfo[] Returns an array of WebInfo objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('w.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?WebInfo
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
