<?php

namespace App\Repository;

use App\Entity\DiscountUsage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DiscountUsage>
 *
 * @method DiscountUsage|null find($id, $lockMode = null, $lockVersion = null)
 * @method DiscountUsage|null findOneBy(array $criteria, array $orderBy = null)
 * @method DiscountUsage[]    findAll()
 * @method DiscountUsage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscountUsageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountUsage::class);
    }

//    /**
//     * @return DiscountUsage[] Returns an array of DiscountUsage objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DiscountUsage
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
