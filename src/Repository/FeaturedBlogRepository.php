<?php

namespace App\Repository;

use App\Entity\FeaturedBlog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FeaturedBlog>
 *
 * @method FeaturedBlog|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeaturedBlog|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeaturedBlog[]    findAll()
 * @method FeaturedBlog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeaturedBlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeaturedBlog::class);
    }

//    /**
//     * @return FeaturedBlog[] Returns an array of FeaturedBlog objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?FeaturedBlog
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
