<?php

namespace App\Repository;

use App\Entity\CartMerge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CartMerge>
 *
 * @method CartMerge|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartMerge|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartMerge[]    findAll()
 * @method CartMerge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartMergeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartMerge::class);
    }

//    /**
//     * @return CartMerge[] Returns an array of CartMerge objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CartMerge
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
