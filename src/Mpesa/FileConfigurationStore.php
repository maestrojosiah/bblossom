<?php 

namespace App\Mpesa;

use App\Mpesa\Config\CacheStoreInterface;
use App\Mpesa\Config\ConfigurationStoreInterface;
use SmoDav\Mpesa\Contracts\ConfigurationStore;


class FileConfigurationStore implements ConfigurationStoreInterface
{
  private $configPath;
  private $cacheStore;

  public function __construct(string $configPath, CacheStoreInterface $cacheStore)
  {
    $this->configPath = $configPath;
    $this->cacheStore = $cacheStore;
  }

  public function get(string $account): array
  {
    // var_dump($this->configPath);
    // die();
    $cacheKey = $account;

    // Check cache first
    $config = $this->cacheStore->get($cacheKey);
    if ($config !== null) {
      return $config;
    }

    // Fallback to file if not cached
    $filePath = $this->configPath . '.php';
    if (!file_exists($filePath)) {
      return [];
    }

    $config = include $filePath;
    $this->cacheStore->set($cacheKey, $config); // Cache retrieved config
    return $config;
  }

  public function set(string $account, mixed $config): void
  {
    $filePath = $this->configPath . '/' . $account . '.php';
    file_put_contents($filePath, '<?php return ' . var_export($config, true) . ';');

    // Clear cache for the specific account
    $cacheKey = $account;
    $this->cacheStore->delete($cacheKey);
  }
  
}
