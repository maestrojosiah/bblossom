<?php 

namespace App\Mpesa;

use App\Mpesa\Config\CacheStoreInterface;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Psr\Cache\CacheItemPoolInterface;
class SymfonyCacheStore implements CacheStoreInterface
{
  private $cachePool;

  public function __construct(CacheItemPoolInterface $cachePool)
  {
    $this->cachePool = $cachePool;
  }

  public function get(string $key): mixed
  {
    return $this->cachePool->getItem($key)->get();
  }

  public function set(string $key, $value, int $ttl = null): void
  {
    $item = $this->cachePool->getItem($key);
    $item->set($value);
    if ($ttl) {
      $item->expiresAfter($ttl);
    }
    $this->cachePool->save($item);
  }

  public function clear(string $key = null)
  {
    if ($key) {
      $this->cachePool->deleteItem($key);
    } else {
      $this->cachePool->clear();
    }
  }

  // Implement the missing delete method
  public function delete(string $key): bool
  {
    if ($key) {
      return $this->cachePool->deleteItem($key);
    }
    return false;
  }
}
