<?php

namespace App\Mpesa\Config;

interface CacheStoreInterface
{
    public function get(string $key): mixed;

    public function set(string $key, mixed $value, int $ttl = null): void;

    public function delete(string $key): bool;
}
