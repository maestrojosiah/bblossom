<?php

namespace App\Mpesa\Config;

interface ConfigurationStoreInterface
{
    public function get(string $key): mixed;

    public function set(string $key, mixed $value): void;
}
