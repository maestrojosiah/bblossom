<?php

namespace App\Form;

use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\ProductColor;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddToCartType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $product = $options['product']; // Access group object passed as an option

        $builder
            ->add('quantity')
            ->add('color', EntityType::class, [
                'class' => ProductColor::class, // Use ProductColor entity
                'choices' => $product->getProductColors(),
                'choice_label' => 'name', // Assuming 'color' property holds the value
                'placeholder' => 'Choose an option',
                'required' => false,
                'empty_data' => null
            ])        
            ->add('add', SubmitType::class, [
                'label'=> 'Add to cart'
            ])
            ->add('addNBuy', SubmitType::class, [
                'label'=> 'Buy Now'
            ])
            // ->add('price_per_unit')
            // ->add('total_price')
            // ->add('discount_amount')
            // ->add('tax_amount')
            // ->add('subtotal')
            // ->add('product_variant')
            // ->add('is_gift')
            // ->add('gift_message')
            // ->add('notes')
            // ->add('shipping_status')
            // ->add('delivery_date')
            // ->add('payment_date')
            // ->add('orderr')
            // ->add('product')
            // ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,
            'product' => null
        ]);

    }
}
