<?php

namespace App\Entity;

use App\Repository\DiscountUsageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DiscountUsageRepository::class)]
class DiscountUsage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'discountUsages')]
    private ?DiscountCode $discount_code = null;

    #[ORM\ManyToOne(inversedBy: 'discountUsages')]
    private ?User $user = null;


    #[ORM\Column]
    private ?\DateTimeImmutable $usage_date = null;

    #[ORM\ManyToOne(inversedBy: 'discountUsages')]
    private ?Orderr $orderr = null;

    #[ORM\Column]
    private ?int $amount = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDiscountCode(): ?DiscountCode
    {
        return $this->discount_code;
    }

    public function setDiscountCode(?DiscountCode $discount_code): static
    {
        $this->discount_code = $discount_code;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getUsageDate(): ?\DateTimeImmutable
    {
        return $this->usage_date;
    }

    public function setUsageDate(\DateTimeImmutable $usage_date): static
    {
        $this->usage_date = $usage_date;

        return $this;
    }

    public function getOrderr(): ?Orderr
    {
        return $this->orderr;
    }

    public function setOrderr(?Orderr $orderr): static
    {
        $this->orderr = $orderr;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): static
    {
        $this->amount = $amount;

        return $this;
    }
}
