<?php

namespace App\Entity;

use App\Repository\DeliveriesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DeliveriesRepository::class)]
class Deliveries
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $optgroup = null;

    #[ORM\Column(length: 255)]
    private ?string $area = null;

    #[ORM\Column(length: 255)]
    private ?string $flat = null;

    #[ORM\Column(length: 255)]
    private ?string $exp = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOptgroup(): ?string
    {
        return $this->optgroup;
    }

    public function setOptgroup(string $optgroup): static
    {
        $this->optgroup = $optgroup;

        return $this;
    }

    public function getArea(): ?string
    {
        return $this->area;
    }

    public function setArea(string $area): static
    {
        $this->area = $area;

        return $this;
    }

    public function getFlat(): ?string
    {
        return $this->flat;
    }

    public function setFlat(string $flat): static
    {
        $this->flat = $flat;

        return $this;
    }

    public function getExp(): ?string
    {
        return $this->exp;
    }

    public function setExp(string $exp): static
    {
        $this->exp = $exp;

        return $this;
    }
}
