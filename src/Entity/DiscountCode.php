<?php

namespace App\Entity;

use App\Repository\DiscountCodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DiscountCodeRepository::class)]
class DiscountCode
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $code = null;

    #[ORM\Column(length: 50)]
    private ?int $percentage = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $validity_start_date = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $validity_end_date = null;

    #[ORM\Column]
    private ?int $max_usage_count = null;

    #[ORM\Column]
    private ?bool $is_active = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\OneToMany(mappedBy: 'discount_code', targetEntity: DiscountUsage::class)]
    private Collection $discountUsages;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'discountCodes')]
    private Collection $product;

    public function __toString(): string
    {
        return (string) $this->code;
    }
    public function __construct()
    {
        $this->discountUsages = new ArrayCollection();
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(int $percentage): static
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getValidityStartDate(): ?\DateTimeImmutable
    {
        return $this->validity_start_date;
    }

    public function setValidityStartDate(\DateTimeImmutable $validity_start_date): static
    {
        $this->validity_start_date = $validity_start_date;

        return $this;
    }

    public function getValidityEndDate(): ?\DateTimeImmutable
    {
        return $this->validity_end_date;
    }

    public function setValidityEndDate(\DateTimeImmutable $validity_end_date): static
    {
        $this->validity_end_date = $validity_end_date;

        return $this;
    }

    public function getMaxUsageCount(): ?int
    {
        return $this->max_usage_count;
    }

    public function setMaxUsageCount(int $max_usage_count): static
    {
        $this->max_usage_count = $max_usage_count;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): static
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection<int, DiscountUsage>
     */
    public function getDiscountUsages(): Collection
    {
        return $this->discountUsages;
    }

    public function addDiscountUsage(DiscountUsage $discountUsage): static
    {
        if (!$this->discountUsages->contains($discountUsage)) {
            $this->discountUsages->add($discountUsage);
            $discountUsage->setDiscountCode($this);
        }

        return $this;
    }

    public function removeDiscountUsage(DiscountUsage $discountUsage): static
    {
        if ($this->discountUsages->removeElement($discountUsage)) {
            // set the owning side to null (unless already changed)
            if ($discountUsage->getDiscountCode() === $this) {
                $discountUsage->setDiscountCode(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): static
    {
        if (!$this->product->contains($product)) {
            $this->product->add($product);
        }

        return $this;
    }

    public function removeProduct(Product $product): static
    {
        $this->product->removeElement($product);

        return $this;
    }

}
