<?php

namespace App\Entity;

use App\Repository\ProductReviewRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductReviewRepository::class)]
class ProductReview
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'productReviews')]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'productReviews')]
    private ?Product $product = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Rating = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $review_title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $review_text = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $review_date = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_verified_purchase = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $response = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $review_status = null;

    public function __toString(): string
    {
        return $this->getRating();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): static
    {
        $this->product = $product;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->Rating;
    }

    public function setRating(?string $Rating): static
    {
        $this->Rating = $Rating;

        return $this;
    }

    public function getReviewTitle(): ?string
    {
        return $this->review_title;
    }

    public function setReviewTitle(?string $review_title): static
    {
        $this->review_title = $review_title;

        return $this;
    }

    public function getReviewText(): ?string
    {
        return $this->review_text;
    }

    public function setReviewText(?string $review_text): static
    {
        $this->review_text = $review_text;

        return $this;
    }

    public function getReviewDate(): ?\DateTimeInterface
    {
        return $this->review_date;
    }

    public function setReviewDate(?\DateTimeInterface $review_date): static
    {
        $this->review_date = $review_date;

        return $this;
    }

    public function isIsVerifiedPurchase(): ?bool
    {
        return $this->is_verified_purchase;
    }

    public function setIsVerifiedPurchase(?bool $is_verified_purchase): static
    {
        $this->is_verified_purchase = $is_verified_purchase;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(?string $response): static
    {
        $this->response = $response;

        return $this;
    }

    public function getReviewStatus(): ?string
    {
        return $this->review_status;
    }

    public function setReviewStatus(?string $review_status): static
    {
        $this->review_status = $review_status;

        return $this;
    }
}
