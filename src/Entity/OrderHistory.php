<?php

namespace App\Entity;

use App\Repository\OrderHistoryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderHistoryRepository::class)]
class OrderHistory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orderHistories')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $order_status = null;

    #[ORM\ManyToOne(inversedBy: 'orderHistories')]
    private ?Address $shipping_address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $payment_method = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $total_amount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shipping_method = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $payment_status = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_of_payment = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $delivery_date = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $promo_code = null;

    #[ORM\Column(nullable: true)]
    private ?float $order_discount = null;

    #[ORM\Column(nullable: true)]
    private ?float $tax_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $subtotal = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $order_date = null;

    #[ORM\Column(nullable: true)]
    private ?float $shipping_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $discount_amount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tracking_number = null;

    public function __toString(): string
    {
        return $this->getUser()->getFullName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getOrderStatus(): ?string
    {
        return $this->order_status;
    }

    public function setOrderStatus(?string $order_status): static
    {
        $this->order_status = $order_status;

        return $this;
    }

    public function getShippingAddress(): ?Address
    {
        return $this->shipping_address;
    }

    public function setShippingAddress(?Address $shipping_address): static
    {
        $this->shipping_address = $shipping_address;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->payment_method;
    }

    public function setPaymentMethod(?string $payment_method): static
    {
        $this->payment_method = $payment_method;

        return $this;
    }

    public function getTotalAmount(): ?float
    {
        return $this->total_amount;
    }

    public function setTotalAmount(?float $total_amount): static
    {
        $this->total_amount = $total_amount;

        return $this;
    }

    public function getShippingMethod(): ?string
    {
        return $this->shipping_method;
    }

    public function setShippingMethod(?string $shipping_method): static
    {
        $this->shipping_method = $shipping_method;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->payment_status;
    }

    public function setPaymentStatus(?string $payment_status): static
    {
        $this->payment_status = $payment_status;

        return $this;
    }

    public function getDateOfPayment(): ?\DateTimeInterface
    {
        return $this->date_of_payment;
    }

    public function setDateOfPayment(?\DateTimeInterface $date_of_payment): static
    {
        $this->date_of_payment = $date_of_payment;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->delivery_date;
    }

    public function setDeliveryDate(?\DateTimeInterface $delivery_date): static
    {
        $this->delivery_date = $delivery_date;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getPromoCode(): ?string
    {
        return $this->promo_code;
    }

    public function setPromoCode(?string $promo_code): static
    {
        $this->promo_code = $promo_code;

        return $this;
    }

    public function getOrderDiscount(): ?float
    {
        return $this->order_discount;
    }

    public function setOrderDiscount(?float $order_discount): static
    {
        $this->order_discount = $order_discount;

        return $this;
    }

    public function getTaxAmount(): ?float
    {
        return $this->tax_amount;
    }

    public function setTaxAmount(?float $tax_amount): static
    {
        $this->tax_amount = $tax_amount;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(?float $subtotal): static
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getOrderDate(): ?\DateTimeInterface
    {
        return $this->order_date;
    }

    public function setOrderDate(?\DateTimeInterface $order_date): static
    {
        $this->order_date = $order_date;

        return $this;
    }

    public function getShippingAmount(): ?float
    {
        return $this->shipping_amount;
    }

    public function setShippingAmount(?float $shipping_amount): static
    {
        $this->shipping_amount = $shipping_amount;

        return $this;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discount_amount;
    }

    public function setDiscountAmount(?float $discount_amount): static
    {
        $this->discount_amount = $discount_amount;

        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->tracking_number;
    }

    public function setTrackingNumber(?string $tracking_number): static
    {
        $this->tracking_number = $tracking_number;

        return $this;
    }
}
