<?php

namespace App\Service;

use App\Mpesa\FileConfigurationStore;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SmoDav\Mpesa\C2B\Registrar;
use SmoDav\Mpesa\C2B\Simulate;
use GuzzleHttp\Client;
use SmoDav\Mpesa\Engine\Core;
use SmoDav\Mpesa\Native\NativeCache;
use SmoDav\Mpesa\Native\NativeConfig;
use SmoDav\Mpesa\C2B\STK;
use App\Entity\Pmt;
use App\Repository\CallBackRepository;
use App\Repository\PmtRepository;
use App\Service\SendSms;

class MPesaManager extends AbstractController
{
    final public const CUSTOMER_BUYGOODS_ONLINE = 'CustomerBuyGoodsOnline';

    public function __construct(private CallBackRepository $cbRepo, private PmtRepository $pmtRepo, private ManagerRegistry $managerRegistry, private FileConfigurationStore $configStore)
    {
    }


    public function auth(): Response
    {
        $config = new NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client(), $config, $cache);
        $authenticator = new \SmoDav\Mpesa\Auth\Authenticator($core);
        $auth = $authenticator->authenticate();
        return $auth;
    }

    // public function reg($conf, $val): Response
    // {
    //     $conf = 'https://new.chezamusicschool.co.ke/mpesa/confirm?secret=secretkey';
    //     $val = 'https://new.chezamusicschool.co.ke/mpesa/validate?secret=secretkey';

    //     $response = Registrar::register(600000)
    //         ->onConfirmation($conf)
    //         ->onValidation($val)
    //         ->submit();

    //     return $response;
    // }

    public function pushSimulate($amt, $number, $ref): Response
    {
        $config = new NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client(), $config, $cache);

        $simulate = new Simulate($core);
        $response = $simulate->request($amt)
        ->from($this->formatPhoneNumber($number))
        ->usingReference($ref)
        // ->setCommand(CUSTOMER_BUYGOODS_ONLINE)
        ->push();

        return $response;

    }

    public function pushSTK($amt, $number, $ref, $desc)
    {
        $defaultAccount = 'mpesa'; // Adjust as needed
        $myconfig = $this->configStore->get($defaultAccount);

        // Access configuration details
        $mode = $myconfig['default'];
        $cache_location = $myconfig['cache_location'];

        $config = new NativeConfig();
        $cache = new NativeCache($cache_location);
        $core = new Core(new Client(), $config, $cache);

        $stk = new STK($core);
        $response = $stk->request($amt)
        ->from($this->formatPhoneNumber($number))
        ->usingReference($ref, $desc)
        ->setCommand('CustomerBuyGoodsOnline')
        ->push();

        $data = $response;
        $errors = [];
        if (isset($data->errorMessage)) {
            if($data->errorMessage == "error on exit from activity, no matching transition") {
                $msg = "Number is invalid. Please use an M-pesa activated number.";
                $errors['error'] = $msg;
            } else {
                $msg = $data->errorMessage;
                $errors['error'] = $msg;
            }
            $ret = ['failed', $msg];

            return $ret;

        } else {

            $pmt = new Pmt();
            $pmt->setMerchantrequestid($data->MerchantRequestID);
            $pmt->setCheckoutrequestid($data->CheckoutRequestID);
            $pmt->setResponsecode($data->ResponseCode);
            $pmt->setResponsedescription($data->ResponseDescription);
            $pmt->setCustomermessage($data->CustomerMessage);
            $this->save($pmt);

            $ret = ['pending', $data->CheckoutRequestID];

            return $ret;

        }


    }

    function getData($payload, $variable) {
        // Convert payload to associative array
        $data = json_decode(json_encode($payload), true);
    
        // Check if the variable exists in the payload
        if (isset($data['data'][$variable])) {
            return $data['data'][$variable];
        } else {
            return null; // Variable not found
        }
    }
    
    public function checkStatus($request_id, $purpose)
    {

        $errors = [];
        $config = new NativeConfig();
        $cache = new NativeCache($config->get('cache_location'));
        $core = new Core(new Client(), $config, $cache);

        $stk = new STK($core);
        $response = $stk->validate($request_id);

        $status = $response;
        // if is error
        if (isset($status->errorMessage)) {

            $msg = $status->errorMessage;
            $errors['error'] = $msg;

            $ret = ['failed', $msg];
            return $ret;
        } else { // if is not error

            $pmt = $this->pmtRepo->findOneByCheckoutrequestid($request_id);
            $pmt->setResultcode($status->ResultCode);
            $pmt->setResultdesc($status->ResultDesc);
            $this->save($pmt);

            if($status->ResultCode == 0) {

                $callback = $this->cbRepo->findOneByCheckoutRequestId($request_id);

                if(null == $callback) {
                    $ret = ['try_again', 'Wait for confirmation message and try again'];
                    return $ret;
                } else {
                    $json = $callback->getCallBackmetadata();

                    $Amount = $this->getValueByKey($json, 'Amount');
                    $MpesaReceiptNumber = $this->getValueByKey($json, 'MpesaReceiptNumber');
                    $TransactionDate = $this->getValueByKey($json, 'TransactionDate');
                    $PhoneNumber = $this->getValueByKey($json, 'PhoneNumber');

                    $callback->setMpesaReceiptNumber($MpesaReceiptNumber);
                    $callback->setTransactionDate($TransactionDate);
                    $callback->setAmount($Amount);
                    $callback->setResultCode($status->ResultCode);
                    $callback->setPhoneNumber($PhoneNumber);

                    $this->save($callback);
                    $msg = "Payment successful:".$Amount;
                    $ret = ['success', $msg];
                    return $ret;

                }

            } else {

                $msg = $status->ResultDesc;
                $errors['error'] = $msg;
                $ret = ['failed', $errors];

                return $ret;
            }
        }

        // return new JsonResponse($status);

    }

    function getValueByKey($decoded_data, $key) {
        // Check if the key exists in stkCallback directly
        if (isset($decoded_data['Body']['stkCallback'][$key])) {
            return $decoded_data['Body']['stkCallback'][$key];
        }
        
        // Check if the key exists in CallbackMetadata
        if (isset($decoded_data['Body']['stkCallback']['CallbackMetadata']['Item'])) {
            foreach ($decoded_data['Body']['stkCallback']['CallbackMetadata']['Item'] as $item) {
                if ($item['Name'] === $key) {
                    return isset($item['Value']) ? $item['Value'] : "$key found but has no value";
                }
            }
        }
        
        // If not found, return appropriate message
        return "$key not found";
    }
    

    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function formatPhoneNumber($phone)
    {

        //The default country code if the recipient's is unknown:
        $country_code  = '254';

        //Remove any parentheses and the numbers they contain:
        $phone = preg_replace("/\([0-9]+?\)/", "", (string) $phone);

        //Strip spaces and non-numeric characters:
        $phone = preg_replace("/[^0-9]/", "", $phone);

        //Strip out leading zeros:
        $phone = ltrim($phone, '0');

        //Look up the country dialling code for this number:
        $pfx = $country_code;

        //Check if the number doesn't already start with the correct dialling code:
        if (!preg_match('/^'.$pfx.'/', $phone)) {
            $phone = $pfx.$phone;
        }

        //return the converted number:
        return $phone;

    }



}
