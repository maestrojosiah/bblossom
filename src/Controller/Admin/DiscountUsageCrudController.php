<?php

namespace App\Controller\Admin;

use App\Entity\DiscountUsage;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DiscountUsageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DiscountUsage::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("discount_code")->setLabel("Discount Code")
            ->autocomplete();
        yield AssociationField::new("user")->setLabel("User");
        yield AssociationField::new("orderr")->setLabel("Order");
        yield IntegerField::new("amount")->setLabel("Amount");

    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Discount Usage")
            ->setEntityLabelInPlural("Discount Usage");

    }

}
