<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BlogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Blog::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Blog();
        $template->setAuthor($this->getUser());
        $template->setPublicationDate(new \DateTime());
        $template->setLastModified(new \DateTime());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setLastModified(new \DateTime());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {
        
        yield FormField::addColumn(6)->setLabel('Blog Information');
        yield TextField::new("title")->setLabel("Blog Title");
        yield AssociationField::new("blogCategories")->setLabel("Blog Categories")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete();
        yield TextEditorField::new("content")->setLabel("Blog Content")
            ->hideOnIndex()
            ->setTemplatePath('admin/fields/raw_content.html.twig')
            ->setFormType(CKEditorType::class);
        yield ImageField::new("featuredImage")->setLabel("Blog Image")
            ->setUploadDir("/public/site/images/blog")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/blog")
            ->hideOnIndex()
            ->setHelp("800 x 394 pixels");

        yield FormField::addColumn(6)->setLabel('SEO');
        yield TextField::new("slug")->setLabel("Slug")
            ->hideOnIndex();
        yield AssociationField::new("tags")->setLabel("Tags")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete();
        yield BooleanField::new("isPublished")->setLabel("Is Published?")
            ->renderAsSwitch(false);
        yield BooleanField::new("isFeatured")->setLabel("Is Featured?")
            ->renderAsSwitch(false);
        yield TextField::new("metaTitle")->setLabel("Meta Title")
            ->hideOnIndex();
        yield TextareaField::new("metaDescription")->setLabel("Meta Description")
            ->hideOnIndex();
        yield TextAreaField::new("keywords")->setLabel("Keywords")
            ->hideOnIndex();
        yield DateField::new("publicationDate")->setLabel("Publication Date")
            ->onlyOnDetail();
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }

}
