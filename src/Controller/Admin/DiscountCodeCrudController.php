<?php

namespace App\Controller\Admin;

use App\Entity\DiscountCode;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DiscountCodeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DiscountCode::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $dc = new DiscountCode();
        $dc->setCreatedAt(new \DateTimeImmutable());

        return $dc;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Discount Codes Info');
        yield TextField::new("code")->setLabel("Code");
        yield IntegerField::new("percentage")->setLabel("Percentage");
        yield IntegerField::new("maxUsageCount")->setLabel("Max Usage");
        yield BooleanField::new("isActive")->setLabel("Is Active?");

        yield FormField::addColumn(6);
        yield AssociationField::new("product")->setLabel("Products")
        ->hideOnIndex()
        ->setTemplatePath('admin/fields/discounted_products.html.twig');
        yield DateTimeField::new("validityStartDate")->setLabel("Valid From:")
            ->setFormTypeOption("input","datetime_immutable");
        yield DateTimeField::new("validityEndDate")->setLabel("Valid To")
            ->setFormTypeOption("input","datetime_immutable");
        yield DateTimeField::new("createdAt")->setLabel("Created On")
            ->hideOnForm();
        
    }


    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Discount Code")
            ->setEntityLabelInPlural("Discount Codes");

    }

}
