<?php

namespace App\Controller\Admin;

use App\Entity\CommentReplies;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommentRepliesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CommentReplies::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Comment Reply Details');
        yield AssociationField::new("comment")->setLabel("Comment")
            ->setTemplatePath('admin/fields/shortened_text.html.twig');
        yield AssociationField::new("user")->setLabel("Author")
            ->autocomplete()
            ->onlyOnDetail();
        yield TextareaField::new("content")->setLabel("Content");
        yield DateField::new("repliedOn")->setLabel("Replied on")
            ->onlyOnDetail();

    }

    public function createEntity(string $entityFqcn)
    {
        $template = new CommentReplies();
        $template->setUser($this->getUser());
        $template->setRepliedOn(new \DateTime());

        return $template;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Comment Reply")
            ->setEntityLabelInPlural("Comment Replies");

    }
}
