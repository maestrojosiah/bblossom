<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class CategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Category::class;
    }


    public function configureFields(string $pageName): iterable
    {

        yield FormField::addColumn(6);
        yield TextField::new("categoryName")->setLabel("Category Name");
        yield TextField::new("slug")->setLabel("Slug");
        yield TextField::new("tagline")->setLabel("Tagline")
            ->setHelp("255 characters maximum")
            ->hideOnIndex();
        yield TextField::new("metaTitle")->setLabel("Search Engine Title")
            ->setHelp("255 characters maximum")
            ->hideOnIndex();
        yield TextField::new("pageTitle")->setLabel("Page Title H1")
            ->setHelp("255 characters maximum")
            ->hideOnIndex();                        
        yield TextField::new("summary")->setLabel("Summary")
            ->setHelp("255 characters maximum")
            ->hideOnIndex();

        yield FormField::addColumn(6);
        yield TextAreaField::new("description")->setLabel("Description")
            ->setFormType(CKEditorType::class)
            ->hideOnIndex()
            ->setTemplatePath('admin/fields/raw_desc.html.twig');        
        yield ImageField::new("imageUrl")->setLabel("Image")
            ->setUploadDir("/public/site/images/uploads")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/uploads");
        yield TextField::new("videoUrl")->setLabel("Video Embed URL")
            ->setHelp("Embed youtube url")
            ->hideOnIndex();
        yield BooleanField::new("isActive")->setLabel("Is Active");
        yield BooleanField::new("isMainCategory")->setLabel("Is Main Category?");
        yield IntegerField::new("displayOrder")->setLabel("DisplayOrder");

    }

    
    public function configureActions(Actions $actions) : Actions
    {
        return parent::configureActions($actions)
        ->disable(Action::DETAIL);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Product Category")
            ->setEntityLabelInPlural("Product Categories")
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');

    }


}
