<?php

namespace App\Controller\Admin;

use App\Entity\ProductDisplay;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductDisplayCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductDisplay::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
    public function configureFields(string $pageName): iterable
    {

        yield FormField::addColumn(6)->setHelp("Images for the homepage slider and product page banners. If you add one for a product, it will be displayed on the product's page top banner");
        yield AssociationField::new("product")->setLabel("Product");
        yield ImageField::new("path")->setLabel("URL to image")
            ->setUploadDir("/public/site/images/product_display")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/product_display")
            ->setHelp("1920 x 477 px (banners), 209 x 584 px (thin)");
        yield ChoiceField::new("label")
            ->setLabel("Type Of Display")
            ->setChoices([
                'Thin' => 'thin',
                'Square' => 'square',
                'Rectangle' => 'rectangle',
                'Circle' => 'circle',
                'Banner' => 'banner',
            ])
            ->renderExpanded(true);
        

    }


    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->overrideTemplate('crud/new', 'admin/theme/form_prod_display.html.twig')
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Product Display Image")
            ->setEntityLabelInPlural("Product Display Images");

    }

}
