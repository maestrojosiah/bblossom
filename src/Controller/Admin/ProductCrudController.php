<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Product();
        $template->setDateAdded(new \DateTime());

        return $template;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addTab("Product Details");
        yield FormField::addColumn(6);
        yield TextField::new("productName")->setLabel("Name");
        yield TextField::new("slug")->setLabel("Slug")
            ->hideOnIndex();
        yield AssociationField::new("categories")->setLabel("Categories")
            ->hideOnIndex();
        yield TextAreaField::new("description")->setLabel("Description")
            ->setFormType(CKEditorType::class)
            ->hideOnIndex()
            ->hideOnIndex()
            ->setTemplatePath('admin/fields/raw_desc.html.twig');
        yield ImageField::new("images")->setLabel("Image")
            ->setUploadDir("/public/site/images/products")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/products")
            ->hideOnIndex();

        yield FormField::addColumn(6);
        yield AssociationField::new("productColors")->setLabel("Product Variety")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete();
        yield TextField::new("videoLink")->setLabel("Video Link")
            ->hideOnIndex();
        yield IntegerField::new("price")->setLabel("Price");
        yield AssociationField::new("productImages")->setLabel("Images");
        // yield IntegerField::new("stockQuantity")->setLabel("In Stock");
        yield TextField::new("images")->setLabel("Images")
            ->onlyOnDetail();
        yield IntegerField::new("rating")->setLabel("Rating")
            ->onlyOnDetail();
        yield IntegerField::new("numberOfRatings")->setLabel("No. Of Ratings")
            ->hideOnIndex()
            ->onlyOnDetail();
        yield BooleanField::new("availability")->setLabel("Is Available?")
            ->renderAsSwitch(false);
        yield TextEditorField::new("features")->setLabel("Product Features")
            ->hideOnIndex()
            ->setFormType(CKEditorType::class);
        yield IntegerField::new("weight")->setLabel("Weight")
            ->hideOnIndex();
        yield DateField::new("dateAdded")->setLabel("Added On")
            ->onlyOnDetail();
            
        yield FormField::addTab("Extra Details");
        yield BooleanField::new("isFeatured")->setLabel("Is Featured?")
            ->renderAsSwitch(false)
            ->hideOnIndex();
        yield BooleanField::new("isNew")->setLabel("Is New?")
            ->renderAsSwitch(false)
            ->hideOnIndex();
        yield BooleanField::new("isBestseller")->setLabel("Is Bestseller?")
            ->renderAsSwitch(false)
            ->hideOnIndex();
        yield BooleanField::new("isOnsale")->setLabel("Is On Sale?")
            ->renderAsSwitch(false)
            ->hideOnIndex();


    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }
    

}
