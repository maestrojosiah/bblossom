<?php

namespace App\Controller\Admin;

use App\Entity\WebInfo;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class WebInfoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WebInfo::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6);
        yield TextField::new("type")->setLabel("Information Type");
        yield TextEditorField::new("content")->setLabel("Content")
            ->hideOnIndex()
            ->setFormType(CKEditorType::class);

    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }

}
