<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserCrudController extends AbstractCrudController
{

    public function __construct(
        public UserPasswordHasherInterface $userPasswordHasher
    ) {}

    
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new User();
        $template->setRegistrationDate(new \DateTime());

        return $template;
    }

    public function configureFields(string $pageName): iterable
    {
        
        yield FormField::addColumn(6);
        yield TextField::new("fullName")->setLabel("Full Name")
            ->hideOnForm();
        yield TextField::new("firstName")->setLabel("First Name")
            ->onlyOnForms();
        yield TextField::new("lastName")->setLabel("Last Name")
            ->onlyOnForms();
        yield EmailField::new("email")->setLabel("Email");
        yield ChoiceField::new("usertype")->setLabel("Usertype")
            ->setChoices([
                'User' => 'user',
                'Admin' => 'admin',
            ]);
;
        yield TextField::new("phoneNumber")->setLabel("Phone Number");
        yield ChoiceField::new("gender")->setLabel("Gender")
            ->setChoices([
                'Male' => 'male',
                'Female' => 'female',
            ])
            ->hideOnIndex();

        yield FormField::addColumn(6);
        yield ImageField::new("profilePicture")->setLabel("Photo")
            ->setUploadDir("/public/site/images/profile_pictures")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/profile_pictures")
            ->hideOnIndex();
        yield ChoiceField::new("status")->setLabel("Status")
            ->setChoices([
                'Active' => 'active',
                'Inactive' => 'inactive',
            ])
            ->hideOnIndex();

        yield BooleanField::new("isVerified")->setLabel("Is Verified");
        yield DateField::new("registrationDate")->setLabel("Registration Date")
            ->hideOnIndex();        
        $roles = ['ROLE_USER', 'ROLE_CONTRIBUTOR', 'ROLE_MODERATOR', 'ROLE_SUPER_ADMIN'];
        yield ChoiceField::new('roles')
            ->setChoices(array_combine($roles, $roles))
            ->allowMultipleChoices()
            ->renderAsBadges()
            ->renderExpanded();
        // yield TextField::new('password')
        //     ->setFormType(RepeatedType::class)
        //     ->setFormTypeOptions([
        //         'type'=> PasswordType::class,
        //         'first_options'=> ['label' => 'Password'],
        //         'second_options'=> ['label' => '(Repeat)'],
        //         'mapped'=> false,
        //     ])
        //     ->setRequired($pageName === Crud::PAGE_NEW)
        //     ->onlyOnForms();
            
    }
    
    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if($this->isGranted('ROLE_SUPER_ADMIN')){
            return $queryBuilder;
        }

        $user = $this->getUser();

        if(!$user instanceof User){
            throw new \LogicException('Curently logged in user not an instance of User.');
        }

        return $queryBuilder
            ->andWhere('entity.id = :id')
            ->setParameter('id', $user->getId());

        // return $queryBuilder;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::NEW, 'ROLE_CONTRIBUTOR')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN');
    }


    // public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    // {
    //     $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
    //     return $this->addPasswordEventListener($formBuilder);
    // }

    // public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    // {
    //     $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
    //     return $this->addPasswordEventListener($formBuilder);
    // }

    // private function addPasswordEventListener(FormBuilderInterface $formBuilder): FormBuilderInterface
    // {
    //     return $formBuilder->addEventListener(FormEvents::POST_SUBMIT, $this->hashPassword());
    // }

    // private function hashPassword() {
    //     return function($event) {
    //         $form = $event->getForm();
    //         if (!$form->isValid()) {
    //             return;
    //         }
    //         $password = $form->get('password')->getData();
    //         if ($password === null) {
    //             return;
    //         }

    //         $hash = $this->userPasswordHasher->hashPassword($this->getUser(), $password);
    //         $form->getData()->setPassword($hash);
    //     };
    // }

}
