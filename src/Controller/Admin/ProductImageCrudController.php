<?php

namespace App\Controller\Admin;

use App\Entity\ProductImage;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductImage::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setHelp("Images for the product page slider. If you add several images for a product, they will all be in the slider at the products page for previewing the product.");
        yield AssociationField::new("product")->setLabel("Product");
        yield AssociationField::new("color")->setLabel("Product Variety");
        yield ImageField::new("path")->setLabel("URL to image")
            ->setUploadDir("/public/site/images/product_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/product_images");
        
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->overrideTemplate('crud/new', 'admin/theme/form_prod_image.html.twig')
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Product Slider Image")
            ->setEntityLabelInPlural("Product Slider Images");

    }

}
