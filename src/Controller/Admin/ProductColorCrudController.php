<?php

namespace App\Controller\Admin;

use App\Entity\ProductColor;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductColorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductColor::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Blog Information');
        yield ColorField::new("color")->setLabel("Color");
        yield TextField::new("name")->setLabel("Variety Label")
            ->setHelp("eg. Red, Orange, Black etc");
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Product Variety")
            ->setEntityLabelInPlural("Product Varieties");

    }


}
