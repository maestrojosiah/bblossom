<?php

namespace App\Controller;

use App\Entity\CallBack;
use App\Mpesa\FileConfigurationStore;
use App\Repository\CallBackRepository;
use App\Repository\OrderrRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MPesaManager;

class MpesaController extends AbstractController
{

    public function __construct(private Mailer $mailer, private AdminUrlGenerator $adminUrlGenerator, private UserRepository $userRepo, private MPesaManager $mPesaManager, private ManagerRegistry $managerRegistry, private CallBackRepository $cbRepo, private OrderrRepository $orderRepo){}

    #[Route('/mpesa', name: 'app_mpesa')]
    public function index(FileConfigurationStore $configStore): Response
    {
        // Assuming you want to retrieve the configuration for the default account
        $defaultAccount = 'mpesa'; // Adjust as needed
        $config = $configStore->get($defaultAccount);

        // Access configuration details
        $mode = $config['default'];
        $paybillNumber = $config['accounts'][$mode]['lnmo']['paybill'];

        $message = 'Configuration for ' . $defaultAccount . ':<br>';
        $message .= 'Mode: ' . $mode . '<br>';
        $message .= 'Paybill Number: ' . $paybillNumber;

        return $this->render('mpesa/index.html.twig', [
            'message' => $message,
        ]);
    }

    #[Route('/make/online/payment/push', name: 'make_payment_from_website')]
    public function stkPush(Request $request)
    {

        // $amount = (int) trim($request->request->get('amount'));
        $amount = (int) trim($_POST['amount']);
        //$amount = 1;
        // $number = trim($request->request->get('number'));
        $number = trim($_POST['phone']);
        $reference = 'reference';
        $description = 'testing payment';
        var_dump([$amount, $number]);
        list($status, $res) = $this->mPesaManager->pushSTK($amount, $number, $reference, $description);
        return new JsonResponse(['status'=> $status,'res'=> $res]);
        // return new JsonResponse([$amount, $number, $reference, $description]);
    }
    
    
    #[Route(path: '/cb/bb/mps', name: 'callback')]
    public function callBack()
    {
        

        if($json = json_decode(file_get_contents("php://input"), true)) {
            $CheckoutRequestID = $this->getValueByKey($json, 'CheckoutRequestID');
            $callback = new CallBack();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $this->save($callback);        
            return new JsonResponse('true');
        } else {
            $json = $_POST;
            $CheckoutRequestID = $this->getValueByKey($json, 'CheckoutRequestID');
            $callback = new CallBack();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $this->save($callback);        
            return new JsonResponse('false');
        }
        

    }

    
    function getValueByKey($decoded_data, $key) {
        // Check if the key exists in stkCallback directly
        if (isset($decoded_data['Body']['stkCallback'][$key])) {
            return $decoded_data['Body']['stkCallback'][$key];
        }
        
        // Check if the key exists in CallbackMetadata
        if (isset($decoded_data['Body']['stkCallback']['CallbackMetadata']['Item'])) {
            foreach ($decoded_data['Body']['stkCallback']['CallbackMetadata']['Item'] as $item) {
                if ($item['Name'] === $key) {
                    return isset($item['Value']) ? $item['Value'] : "$key found but has no value";
                }
            }
        }
        
        // If not found, return appropriate message
        return "$key not found";
    }
    

    #[Route('/checkout/confirm/payment/{amountToPay}/{order_id}/{status}/{request_id}/{shipping}', name: 'checkout_confirm_payment')]
    public function confirmPayment($amountToPay, $order_id, $status, $request_id, $shipping): Response
    {

        return $this->render('cart/pay.html.twig', [
            'amountToPay' => $amountToPay,
            'order_id' => $order_id,
            'status' => $status,
            'request_id' => $request_id,
            'shipping' => $shipping
        ]);


    }

    #[Route('/checkout/complete/payment', name: 'checkout_complete_payment')]
    public function completePayment(EntityManagerInterface $em)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            $request_id = $_POST['request_id'];
            $purpose = "BB-ORDER-" . $_POST['order_id'];
            $order = $this->orderRepo->find($_POST['order_id']);
            $shipping_cost = $_POST['shipping'];
            $amountToPay = $_POST['amountToPay'];
    
            list($status, $res) = $this->mPesaManager->checkStatus($request_id, $purpose);

            if($status == 'success'){
                $paymentStatus = "successful";
                $this->followUp($request_id);
                $order->setOrderStatus('processing');
                $order->setDeliveryStatus('in_transit');
                $order->setPaymentStatus('completed');
                $order->setPaymentDate( new \DateTime() );
                $this->save($order);
                // send emails
                $user = $order->getUser();
                $msg = "New Order: Name: " . $user->getFullName() . " Phone Number: " . $user->getPhoneNumber() . ".";
                $subject = "New Paid Order From Website";
                $intro = "There is a new paid order from the website.";
                $admins = $this->userRepo->findByUsertype('admin');
                $urlToOrderDetail = $this->adminUrlGenerator->setController('App\Controller\Admin\OrderrCrudController')
                ->setAction('detail')
                ->setEntityId($order->getId())
                ->generateUrl();
                $maildata = ['name' => $user->getFullName(), 'emailAd' => $user->getEmail(), 'subject' => $subject, 'shipping_cost' => $shipping_cost, 'cartAndShipping' => $amountToPay, 'message' => $msg, 'order' => $order, 'urlToOrderDetail' => $urlToOrderDetail, 'intro' => $intro];
                
                foreach ($admins as $admin) {
                    $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "orderform.html.twig");
                }
                $this->mailer->sendEmailMessage($maildata, $user->getEmail(), "Your order has been received", "order_received.html.twig");
        
                $this->addFlash('success','Your Order has been received. We will call you soon');
        

                return $this->redirectToRoute('order_received', ['order_id' => $order->getId(), 'shipping' => $shipping_cost]);
            } else {
                $paymentStatus = "not successful";
                return $this->render('cart/status.html.twig', [
                    'status' => $status,
                    'paymentStatus' => $paymentStatus,
                    'request_id' => $request_id,
                ]);
            }

        

        }


    }

    #[Route('/pmt/stk_push/check/status', name: 'leepahnapush_status')]
    public function checkStatus(Request $request) {

        $request_id = $request->request->get('request_id');
        $purpose = $request->request->get('purpose');

        list($status, $res) = $this->mPesaManager->checkStatus($request_id, $purpose);
        if($status == 'success'){
            $this->followUp($request_id);
        }
        return new JsonResponse(['status' => $status, 'response' => $res]);


    }

    public function followUp($request_id){

        $callback = $this->cbRepo->findOneByCheckoutRequestId($request_id);

        $json = $callback->getCallbackmetadata();

        $Amount = $this->getValueByKey($json, 'Amount');
        $MpesaReceiptNumber = $this->getValueByKey($json, 'MpesaReceiptNumber');
        $TransactionDate = $this->getValueByKey($json, 'TransactionDate');
        $PhoneNumber = $this->getValueByKey($json, 'PhoneNumber');
    
        $callback->setMpesaReceiptNumber($MpesaReceiptNumber);
        $callback->setTransactionDate($TransactionDate);
        $callback->setAmount($Amount);
        $callback->setPhoneNumber($PhoneNumber);
        $this->save($callback);        

        return new JsonResponse('true');

    }


    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

}
