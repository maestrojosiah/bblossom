<?php

namespace App\Controller;

use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\ProductReview;
use App\Form\AddToCartType;
use App\Form\ProductReviewType;
use App\Manager\CartManager;
use App\Repository\ProductDisplayRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{

    public function __construct(private readonly SeoGeneratorProvider $seoGenerator, private readonly EntityManagerInterface $em)
    {}

    #[Route('/product', name: 'app_product')]
    public function index(ProductRepository $productRepo): Response
    {
        $this->seoGenerator->get('basic')
            ->setTitle('Baby Skin Care Essentials For Eczema and Sensitive Skin')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. These products are entirely natural and have earned the trust of Kenyan families, making Baby Blossom one of the most reputable choice for both infants and mothers. Baby Blossom offers a range of 100% natural baby care products, including Calendula baby soap, baby lotion, baby wash and shampoo, and massage oil.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');

        $products = $productRepo->findBy(
            ['availability' => true]
        );
        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/shop', name: 'app_shop')]
    public function shop(ProductRepository $productRepo): Response
    {
        $this->seoGenerator->get('basic')
            ->setTitle('Baby Skin Care Essentials For Eczema and Sensitive Skin')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. These products are entirely natural and have earned the trust of Kenyan families, making Baby Blossom one of the most reputable choice for both infants and mothers. Baby Blossom offers a range of 100% natural baby care products, including Calendula baby soap, baby lotion, baby wash and shampoo, and massage oil.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');

        $products = $productRepo->findBy(
            ['availability' => true]
        );
        return $this->render('product/shop.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/shop/category/{cagegory_id}', name: 'app_for_category')]
    public function shop_category(ProductRepository $productRepo, $category_id): Response
    {
        $this->seoGenerator->get('basic')
            ->setTitle('Baby Skin Care Essentials For Eczema and Sensitive Skin')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. These products are entirely natural and have earned the trust of Kenyan families, making Baby Blossom one of the most reputable choice for both infants and mothers. Baby Blossom offers a range of 100% natural baby care products, including Calendula baby soap, baby lotion, baby wash and shampoo, and massage oil.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');

        $products = $productRepo->findBy(
            ['availability' => true]
        );
        return $this->render('product/shop.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/add/product/to/cart/index', name: 'add_product_to_cart_index')]
    public function addToCartFromIndex(ProductRepository $productRepo, CartManager $cartManager, Request $request)
    {

        $product_id = $request->request->get('product_id');
        $clickedButton = $request->request->get('clickedButton');


        $product = $productRepo->findOneById($product_id);

        $clickedButton = $request->request->get('clickedButton'); // Get the button name from AJAX data
            
        $item = new OrderItem();

        // if returning customer, give discount
        // calculate tax
        $tax = 0;

        $item->setQuantity(1);
        $item->setProduct($product);
        $item->setPricePerUnit($product->getPrice());
        $item->setTotalPrice($item->calculateTotalPrice());
        $item->setTaxAmount($tax);
        $item->setSubtotal($item->calculateSubtotal());
        // $item->setProductVariant();
        // $item->setIsGift();
        // $item->setGiftMessage();
        // $item->setNotes();
        $item->setShippingStatus('processing');
        if(null !== $this->getUser()) {
            $item->setUser($this->getUser());
        }
        
        $this->em->persist($item);
        $this->em->flush();

        // $item->setDeliveryDate();
        // $item->setPaymentDate();

        
        $cart = $cartManager->getCurrentCart();

        $currentDate = date('ymd');
        // Create the tracking number by concatenating the date and order ID
        $trackingNumber = $currentDate .'-'. rand(1000, 9999);
        $cart
            ->addOrderItem($item)
            ->setOrderDate(new \DateTime())
            ->setPaymentMethod('mpesa')
            // ->setTotalAmount('') this will be set at the last stage of checkout
            // ->setTaxAmount
            // ->setShippingAmount
            // ->setSubtotal
            // ->setDiscountAmount
            ->setTrackingNumber($trackingNumber)
            // ->setShippingMethod
            ->setPaymentStatus('pending')
            // ->setPaymentDate
            // ->setNotes
            // ->setPromoCode
            ->setDeliveryStatus('not_shipped')
            // ->setOrderDate
            // ->setQuantity
            ;

        if(null !== $this->getUser()) {
            $cart->setUser($this->getUser());
        }
        $cartManager->save($cart);

        return new JsonResponse(['status' => 'success', 'message' => 'addNBuy']);

    }

    #[Route('/product/{product_slug}', name:'product_show')]
    public function show(Request $request, CartManager $cartManager, ProductRepository $productRepo, ProductDisplayRepository $productDisplayRepo, $product_slug): Response
    {
        $product = $productRepo->findOneBySlug($product_slug);
        $review = new ProductReview();

        $categories = [];
        $productCategories = $product->getCategories();
        foreach( $productCategories as $category ) {
            if($category->getCategoryName() !== 'Gift Sets') {
                $categories[] = $category->getCategoryName();
            }

        }

        $randomCategories = array_rand($categories, 1); // Pick 1 random indexes
        $randomCategoryName = $categories[$randomCategories];
        $relatedProducts = $productRepo->findByCategoryName($randomCategoryName);
        
        $banner = $productDisplayRepo->findOneBy(
            ['label' => 'banner', 'product' => $product],
            ['id'=> 'ASC']
        );
    
        $form = $this->createForm(AddToCartType::class, null, [
            'product'=> $product,
        ]);
        
        $review_form = $this->createForm(ProductReviewType::class, $review);

        $form->handleRequest($request);
        $review_form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $clickedButton = $request->request->get('clickedButton'); // Get the button name from AJAX data
                
            $item = $form->getData();

            // calculate tax
            $tax = 0;

            $item->setProduct($product);
            $item->setPricePerUnit($product->getPrice());
            $item->setTotalPrice($item->calculateTotalPrice());
            $item->setTaxAmount($tax);
            $item->setSubtotal($item->calculateSubtotal());
            // $this->em->persist($item);
            // $item->setProductVariant();
            // $item->setIsGift();
            // $item->setGiftMessage();
            // $item->setNotes();
            $item->setShippingStatus('processing');
            if(null !== $this->getUser()) {
                $item->setUser($this->getUser());
            }
            
            // $item->setDeliveryDate();
            // $item->setPaymentDate();
    
            
            $cart = $cartManager->getCurrentCart();

            $currentDate = date('ymd');
            // Create the tracking number by concatenating the date and order ID
            $trackingNumber = $currentDate .'-'. rand(1000, 9999);
            $cart
                ->addOrderItem($item)
                ->setOrderDate(new \DateTime())
                ->setPaymentMethod('mpesa_on_delivery')
                // ->setTotalAmount('') this will be set at the last stage of checkout
                // ->setTaxAmount
                // ->setShippingAmount
                // ->setSubtotal
                // ->setDiscountAmount
                ->setTrackingNumber($trackingNumber)
                // ->setShippingMethod
                ->setPaymentStatus('pending')
                // ->setPaymentDate
                // ->setNotes
                // ->setPromoCode
                ->setDeliveryStatus('not_shipped')
                // ->setOrderDate
                // ->setQuantity
                ;

            if(null !== $this->getUser()) {
                $cart->setUser($this->getUser());
            }
            $cartManager->save($cart);
            $totalProducts = count($cart->getOrderItems());
            $orderItems = $cart->getOrderItems();
            $totalPrice = $cart->getTotal();

            $products = [];
            foreach($orderItems as $orderItem) {
                $products[] = ['name'=> $orderItem->getProduct()->getProductName(),'quantity'=> $orderItem->getQuantity(), 'total' => $orderItem->getTotal(), 'images' => $orderItem->getProduct()->getImages(), 'slug' => $orderItem->getProduct()->getSlug()];
            }
            // // return $this->redirectToRoute('product_show', ['product_slug' => $product->getSlug()]);
            if ($clickedButton === 'add_to_cart[add]') {
                // 'Add' button was clicked
                // Your logic for 'Add' button here
                return new JsonResponse(['status' => 'success', 'message' => 'add', 'totalProducts' => $totalProducts,'totalPrice'=> $totalPrice, 'products' => $products]);
            } elseif ($clickedButton === 'add_to_cart[addNBuy]') {
                // 'Buy Now' button was clicked
                // Your logic for 'Buy Now' button here
                return new JsonResponse(['status' => 'success', 'message' => 'addNBuy']);
            } else {
                return new JsonResponse(['status' => 'Failed', 'message' => 'I dont know what happened']);
            }

        }

        if ($review_form->isSubmitted() && $review_form->isValid()) {

            $entityManager = $this->em;
            $review->setReviewTitle("Customer Review");
            $review->setReviewDate(new \DateTime());
            $review->setReviewStatus("in_review");
            $review->setUser($this->getUser());
            $review->setProduct($product);
            $entityManager->persist($review);
            $entityManager->flush();
    
            // Redirect or do something else after successful form submission
            return $this->redirectToRoute('product_show', ['product_slug' => $product_slug]);

        }

        $this->seoGenerator->get('basic')
            ->setTitle($product->getProductName())
            ->setDescription(substr(strip_tags($product->getDescription()), 0, 165))
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');
            
        $latestDiscountCode = $product->getLatestDiscountCode();

        return $this->render('product/show.html.twig', [
            'product' => $product,
            'products' => $productRepo->findAll(),
            'banner' => $banner,
            'form'=> $form->createView(),
            'review_form'=> $review_form->createView(),
            'relatedProducts' => $relatedProducts,
            'latestDiscountCode' => $latestDiscountCode,
        ]);
    }
}
