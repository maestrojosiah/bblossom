<?php

namespace App\Controller;

use App\Entity\CallBack;
use App\Repository\CallBackRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class KopoKopoController extends AbstractController
{

    public function __construct(private CallBackRepository $cbRepo, private UserRepository $userRepo, private ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/kopo/kopo', name: 'app_kopo_kopo')]
    public function index(): Response
    {
        return $this->render('kopo_kopo/index.html.twig', [
            'controller_name' => 'KopoKopoController',
        ]);
    }

    #[Route(path: '/cb/bb/kpkp', name: 'callback')]
    public function callBack()
    {
        $payload = [
            "status" => "success",
            "data" => [
              "id" => "cac95329-9fa5-42f1-a4fc-c08af7b868fb",
              "type" => "incoming_payment",
              "initiationTime" => "2018-06-20T22:45:12.790Z",
              "status" => "Success",
              "eventType" =>"Incoming Payment Request",
              "resourceId" => "52f86f-f2aa-4d70-baa2-ccfe4b78f4fc",
              "reference" => '123456789',
              "originationTime" => "2017-01-20T22:45:12.790Z",
              "senderPhoneNumber" => "+2549703119050",
              "amount" => 20000,
              "currency" => "KES",
              "tillNumber" => "K000000",
              "system" => "Lipa Na M-PESA",
              "resourceStatus" => "Received",
              "senderFirstName" => "John",
              "senderMiddleName" => "O",
              "senderLastName" => "Doe",
              "errors"  => [],
              "metadata" => [
                "customer_id" => "123456789",
                "reference" => "123456",
                "notes" => "Payment for invoice 123456"
              ],
              "linkSelf" => "https://sandbox.kopokopo.com/payment_request_results/cac95329-9fa5-42f1-a4fc-c08af7b868fb",
              "callbackUrl" => "https://webhook.site/fa3645c6-7199-426a-8efa-98e7b754babb",
            ]
            ];
          
        if($payload = json_decode(file_get_contents("php://input"), true, 512)) {
            $status = $this->getIncomingPaymentData($payload, 'status');
            $callback = new CallBack();
            $callback->setCallBackData($payload);
            $callback->setStatus($status);
            $this->save($callback);
        };

        return new JsonResponse($this->getIncomingPaymentData($payload, 'status'));

    }

    function getIncomingPaymentData($payload, $variable) {
        // Convert payload to associative array
        $data = json_decode(json_encode($payload), true);
    
        // Check if the variable exists in the payload
        if (isset($data['data'][$variable])) {
            return $data['data'][$variable];
        } else {
            return null; // Variable not found
        }
    }
    
    public function save($entity)
    {
        $entityManager = $this->managerRegistry->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }


}
