<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(private readonly SeoGeneratorProvider $seoGenerator){}

    #[Route(path: '/security/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser() && $this->getUser()->getUserType() != 'admin') {
            return $this->redirectToRoute('cart');
        } elseif ($this->getUser() && $this->getUser()->getUserType() == 'admin') {
            return $this->redirectToRoute('admin');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $this->seoGenerator->get('basic')
            ->setTitle('Login to help us remember you.')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. When you log in, we will be able to apply all discounts that we promised. You have the option of creating an account if you dont have one.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/security/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
