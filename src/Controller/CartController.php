<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\DiscountUsage;
use App\Entity\Message;
use App\Entity\User;
use App\Form\CartType;
use App\Manager\CartManager;
use App\Repository\AddressRepository;
use App\Repository\DeliveriesRepository;
use App\Repository\DiscountCodeRepository;
use App\Repository\DiscountUsageRepository;
use App\Repository\OrderrRepository;
use App\Repository\ProductColorRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use App\Service\MPesaManager;
use Doctrine\ORM\EntityManagerInterface;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class CartController extends AbstractController
{

    public function __construct(private MPesaManager $mpesa, private DiscountUsageRepository $duRepo, private SeoGeneratorProvider $seoGenerator, private DeliveriesRepository $deliveriesRepository, private EntityManagerInterface $em, private UserRepository $userRepo, private Mailer $mailer, private AdminUrlGenerator $adminUrlGenerator)
    {}

    #[Route('/cart', name: 'cart')]
    public function index(CartManager $cartManager, Request $request): Response
    {
        $cart = $cartManager->getCurrentCart($this->getUser());
        
        $form = $this->createForm(CartType::class, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $cart = $form->getData();
            // echo "<pre> from cart index";
            // var_dump($cart->getId());
            // print_r(count($form->getData()->getOrderItems()) );
            foreach ($form->getData()->getOrderItems() as $item) {
                // echo $item->getQuantity()."<br>";
                $totalPrice = $this->calculateTotalPrice($item->getQuantity(), $item->getPricePerUnit());
                $subtotal = $this->calculateSubtotal($totalPrice, 0);
                $item->setTotalPrice($totalPrice);
                $item->setSubtotal($subtotal);
                // print_r($totalPrice);
                // print_r($subtotal);
                // echo "<br>";
            }
            // die();
            // die();
            $cart->setOrderDate(new \DateTime());
            $cartManager->save($cart);

            return $this->redirectToRoute('cart');
        }

        $this->seoGenerator->get('basic')
            ->setTitle('Your Cart')
            ->setDescription('Complete your order')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');
       
        if ($cart->getOrderItems()->count() < 1) {
            return $this->redirectToRoute('app_default');
        }
    
        return $this->render('cart/index.html.twig', [
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }


    #[Route('/update/cart/shipping/cost', name: 'update_order_shipping_cost')]
    public function updateShippingCost(OrderrRepository $orderrRepository, CartManager $cartManager, Request $request)
    {

        $shipping_cost = $request->request->get('shipping_cost');
        $order_id = $request->request->get('order_id');
            
        $order = $orderrRepository->find($order_id);

        $order->setShippingAmount($shipping_cost);
        
        $this->em->persist($order);
        $this->em->flush();

        return new JsonResponse(['shipping_cost' => $shipping_cost, 'totalAmount' => $order->getTotalAmount()]);

    }

    #[Route('/update/cart/discount/amount', name: 'update_order_discount_amount')]
    public function updateDiscount(OrderrRepository $orderrRepository, DiscountCodeRepository $dcRepo, DiscountUsageRepository $duRepo, Request $request)
    {

        // get variables from form
        $discount_code_id = $request->request->get('discountCode');
        $order_id = $request->request->get('order_id');

        // get the entities using ids
        $order = $orderrRepository->find($order_id);
        $discountCode = $dcRepo->findOneByCode($discount_code_id);
    
        $res = $this->validateCode($discountCode, $order);
        // if the code is valid
        if ($res === "passed"){


            // see if there are products associated with this discountCode
            $productsInDiscountCode = $discountCode->getProduct();

            // If there are no products associated with the discount code, apply discount to the entire order
            if ($productsInDiscountCode === null || $productsInDiscountCode->isEmpty()) {
                $discountUsage = $this->findOrCreateDiscountUsage($discountCode, $order);

                $discountAmt = $this->addOtherDiscountsIfExists($order, $discountCode, $discountUsage, null);
                $info = "<span style='color:green'>Discount successfully applied</span>";
                $thisDiscount = $this->calculateDiscount($discountCode->getPercentage(), $order->getTotal(), null);
                $order->setDiscountAmount($discountAmt);
                $discountUsage->setAmount($thisDiscount);
                $this->em->persist($discountUsage);
                $this->em->persist($order);
                $this->em->flush();
        
                $discountAmt = $order->getDiscountAmount();
                $totalAmount = $order->getTotalAmount();
            
                return new JsonResponse(['value' => $discountAmt, 'totalAmount' => $totalAmount, 'info' => $info]);
            } else {
                // find or create discount code
                $discountUsage = $this->findOrCreateDiscountUsage($discountCode, $order);

                $discountAmt = $this->addOtherDiscountsIfExists($order, $discountCode, $discountUsage, $productsInDiscountCode);
                $info = "<span style='color:green'>Products Discounted Successfully</span>";
                $this->em->persist($discountUsage);
                $this->em->persist($order);
                $this->em->flush();
        
                $discountAmt = $order->getDiscountAmount();
                $totalAmount = $order->getTotalAmount();
            
                return new JsonResponse(['value' => $discountAmt, 'totalAmount' => $totalAmount, 'info' => $info]);

            }

    
        } else {
            return $res;
        }
    

    }

    private function findOrCreateDiscountUsage($discountCode, $order)
    {
        // see if there's usage for this order, user and code
        $discountUsage = $this->duRepo->findOneBy(
            ['user' => $this->getUser(), 'orderr' => $order, 'discount_code' => $discountCode],
            ['id' => 'DESC']
        );
    
    
        // if discountUsage is not available for this user, order and code, create new and apply discount
        if (!$discountUsage) {
            $discountUsage = $this->createDiscountUsage($discountCode, $order);
        }
        
        return $discountUsage;
    }
    
    private function addOtherDiscountsIfExists($order, $discountCode, $discountUsage, $productsInDiscountCode)
    {

        // check if there is another discount for this order
        $otherDiscounts = $this->duRepo->findBy(
            ['user' => $this->getUser(), 'orderr' => $order],
            ['id' => 'DESC']
        );
    
        // if there are discounts add them up
        if (count($otherDiscounts) > 0) {
            $otherDiscountsAmount = $this->calculateTotalDiscount($otherDiscounts, $discountUsage);
        } else {
            $otherDiscountsAmount = 0;
        }
        
        // Apply discount only to the products associated with the discount code
        if(null === $productsInDiscountCode){

            $discountAmt = $this->calculateDiscount($discountCode->getPercentage(), $order->getTotal(), $otherDiscountsAmount);


        } else {

            $discountAmt = 0;
            foreach ($order->getOrderItems() as $item) {
                $product = $item->getProduct();
                if ($this->isAmongDiscountedProducts($productsInDiscountCode, $product)) {
                    // Apply discount to this product
                    $thisSingleDiscount = $this->calculateDiscount($discountCode->getPercentage(), $product->getPrice(), null);
                    $thisDiscount = $thisSingleDiscount * $item->getQuantity();
                    $discountUsage->setAmount($thisDiscount);
                    $totalCost = $product->getPrice() * $item->getQuantity();
                    $productDiscount = $this->calculateDiscount($discountCode->getPercentage(), $totalCost, $otherDiscountsAmount);
                    $item->setDiscountAmount($productDiscount);
                    $this->em->persist($discountUsage);    
                    $this->em->persist($item);
                    $discountAmt += $productDiscount * $item->getQuantity();
                }
            }

    
    
        }
        $order->setDiscountAmount($discountAmt);
        $this->em->persist($order);
        $this->em->flush();

        return $discountAmt;

    }
    private function createDiscountUsage($discountCode, $order)
    {
        // create new discountUsage
        $discountUsage = new DiscountUsage();
        $discountUsage->setUser($this->getUser());
        $discountUsage->setDiscountCode($discountCode);
        $discountUsage->setOrderr($order);
        $discountUsage->setUsageDate(new \DateTimeImmutable());


        $thisDiscount = $this->calculateDiscount($discountCode->getPercentage(), $order->getTotal(), null);

        $discountUsage->setAmount($thisDiscount);

        $this->em->persist($discountUsage);
        $this->em->flush();

        return $discountUsage;
        
    }
    private function validateCode($discountCode, $order){
        // if there's no such discount code return 0
        if (!$discountCode) {
            $info = "<span style='color:red'>This discount code does not exist</span>";
            return new JsonResponse(['value' => 0, 'totalAmount' => $order->getTotalAmount(), 'info' => $info]);
        }

        // Check if the discount is active and within validity dates
        $currentDate = new \DateTimeImmutable();

        if (!$discountCode->isIsActive() ) {
            $info = '<span style="color:red">This discount code is not active</span>';
            return new JsonResponse(['value' => 0, 'totalAmount' => $order->getTotalAmount(), 'info' => $info]);
        }

        if ($currentDate < $discountCode->getValidityStartDate()) {
            $info = '<span style="color:red">This offer has not yet started</span>';
            return new JsonResponse(['value' => 0, 'totalAmount' => $order->getTotalAmount(), 'info' => $info]);
        }
        
        if ($currentDate > $discountCode->getValidityEndDate()) {
            $info = '<span style="color:red">This discount is expired</span>';
            return new JsonResponse(['value' => 0, 'totalAmount' => $order->getTotalAmount(), 'info' => $info]);
        }

        return "passed";

    }
    public function isAmongDiscountedProducts($products, $product){
        $productId = $product->getId();
        foreach ($products as $product) {
            if ($product->getId() === $productId) {
                // Product is applicable for the discount code
                return true;
            }
        }
        
        // Product not found in discount code's associated products
        return false;
        
    }
    public function calculateTotalDiscount($discounts, $thisDiscount)
    {
        $totalDiscount = 0;
        foreach ($discounts as $discount) {
            if($discount->getId() !== $thisDiscount->getId()){
                $totalDiscount += $discount->getAmount();
            }
        }
        return $totalDiscount;
    }
    
    public function calculateDiscount($percentage, $amount, $otherDiscountsAmount)
    {
        // Check if the percentage is valid (between 0 and 100)
        if ($percentage < 0 || $percentage > 100) {
            throw new \InvalidArgumentException("Percentage must be between 0 and 100");
        }
    
        // Calculate the discount amount
        $discount = ($percentage / 100) * $amount;
    
        if (null === $otherDiscountsAmount) {
            return $discount;
        }
    
        $totalDiscount = $discount + $otherDiscountsAmount;
    
        // Return the discounted amount
        return $totalDiscount;
    }
    
    #[Route('/update/cart/is/express', name: 'update_order_express')]
    public function updateExpress(OrderrRepository $orderrRepository, CartManager $cartManager, Request $request)
    {

        $is_express = $request->request->get('isExpress');
        $order_id = $request->request->get('order_id');
            
        $order = $orderrRepository->find($order_id);

        if($is_express == "false") {
            $order->setExpress(false);
        } else {
            $order->setExpress(true);
        }
        
        $this->em->persist($order);
        $this->em->flush();

        return new JsonResponse([$is_express, $order_id]);

    }

    #[Route('/checkout', name:'checkout')]
    public function checkout(CartManager $cartManager, DiscountUsageRepository $duRepo, ProductColorRepository $repo, Request $request): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            
            $this->addFlash(
                'success',
                'Having an account will help us know when you return, so that we can always give you a 10% discount.'
            );
    

        }

        $user = $this->getUser();
        if(null !== $user && $user->getUsertype() == 'admin'){
            return $this->redirectToRoute('admin');
        }

        $this->denyAccessUnlessGranted('ROLE_USER');
        $cart = $cartManager->getCurrentCart($this->getUser());

        $deliveries = $this->deliveriesRepository->findAll();
        $optgroups = [];

        foreach ($deliveries as $delivery) {
            $optgroups[$delivery->getOptgroup()][$delivery->getId()] = $delivery;
        }
        
        $discountUsage = $duRepo->findOneBy(
            ['user' => $this->getUser(), 'orderr' => $cart],
            ['id' => 'DESC']
        );
        
        if ($discountUsage) {
            $discountCode = $discountUsage->getDiscountCode();
        } else {
            $discountCode = null;
        }

        $this->seoGenerator->get('basic')
            ->setTitle('Checkout')
            ->setDescription('Checkout')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');
            
        return $this->render('cart/checkout.html.twig', [
            'discountCode'=> $discountCode,
            'cart'=> $cart,
            'user' => $this->getUser(),
            'deliveries' => $deliveries,
            'optgroups' => $optgroups
        ]);

    }

    #[Route('/order/received/successfully/{order_id}/{shipping}', name:'order_received')]
    public function orderReceived(CartManager $cartManager, OrderrRepository $orderRepo, Request $request, $order_id, $shipping): Response
    {

        $this->denyAccessUnlessGranted('ROLE_USER');
        $cart = $orderRepo->findOneById($order_id);


        $this->seoGenerator->get('basic')
            ->setTitle('Order Received')
            ->setDescription('Your order has been successfully received. We will give you a call shortly')
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');
            
        return $this->render('cart/order_received.html.twig', [
            'cart'=> $cart,
            'shipping' => $shipping,
            'user' => $this->getUser(),
        ]);

    }

    public function calculateTotalPrice($quantity, $price_per_unit, $tax_amount = null): ?float
    {
        if ($quantity === null || $price_per_unit === null) {
            return null;
        }

        $subtotal = $quantity * $price_per_unit;

        // Include tax if tax amount is set
        if ($tax_amount !== null) {
            $subtotal += $tax_amount;
        }

        return $subtotal;
    }

    public function calculateSubtotal($total_price, $discount_amount, $tax_amount = null): ?float
    {
        if ($total_price === null || $discount_amount === null) {
            return null;
        }

        $subtotal = $total_price - $discount_amount;

        // Exclude tax if tax amount is set
        if ($tax_amount !== null) {
            $subtotal -= $tax_amount;
        }

        return $subtotal;
    }

    #[Route('/submit/checkout', name:'submit_checkout')]
    public function submitCheckout(CartManager $cartManager, Request $request, AddressRepository $addressRepo, OrderrRepository $orderRepo): Response
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
            // Check which submit button was clicked
            if (isset($_POST['payNow'])) {

                $this->denyAccessUnlessGranted('ROLE_USER');
                $date = new \DateTime();
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $streetAddress = $_POST['streetAddress'];
                $shipping_cost = $_POST['shipping'];
                $cartAndShipping = $_POST['cartAndShipping'];
                $county = $_POST['county'];
                $route = $_POST['route'];
                $phoneNumber = $_POST['phoneNumber'];
                $fullname = $firstName . ' ' . $lastName;
                $user = $this->getUser();
                $user->setPhonenumber($phoneNumber);

                $existingAddress = $addressRepo->findOneByUser($user);
                
                if($existingAddress === null) {
                    $address = new Address();
                } else {
                    $address = $existingAddress;
                }
                
                $address->setUser($user);
                $address->setStreetAddress($streetAddress);
                $address->setCounty($county);
                $this->em->persist($address);
                $this->em->persist($user);
                $this->em->flush();

                $cart = $cartManager->getCurrentCart($this->getUser());
                
                // $cart->setOrderStatus('processing');

                $cart->setTotalAmount($cartAndShipping);
                $cart->setTaxAmount(0);
                $cart->setShippingAmount($shipping_cost);
                $cart->setSubtotal($cart->getTotal());
                $cart->setShippingMethod('delivery');
                $cart->setPaymentStatus('pending'); // update this upon payment

                $cart->setDeliveryStatus('not_shipped'); // update this to in_transit upon payment
                $cart->setShippingAddress($address);
                $cart->setUser($this->getUser());
                
                $msg = "A client has proceeded to payment page: Name: $fullname, Street Address: $streetAddress, County: $county, Route: $route, Phone Number: $phoneNumber. If the order is not completed, they may be experiencing payment issues.";

                $message = new Message();
                $message->setName($fullname);
                $message->setEmail($user->getEmail());
                $message->setMessage($msg);
                $message->setReceivedOn($date);
                $this->em->persist($message);
                $this->em->flush();
                
                $order = $orderRepo->findOneById($cart->getId());

    
                $subject = "A client proceeded to payment page";
                $intro = "A client is on the payment page.";
                $admins = $this->userRepo->findByUsertype('admin');
                $urlToOrderDetail = $this->adminUrlGenerator->setController('App\Controller\Admin\OrderrCrudController')
                    ->setAction('detail')
                    ->setEntityId($cart->getId())
                    ->generateUrl();
                $maildata = ['name' => $fullname, 'emailAd' => $user->getEmail(), 'subject' => $subject, 'shipping_cost' => $shipping_cost, 'cartAndShipping' => $cartAndShipping, 'message' => $msg, 'order' => $order, 'urlToOrderDetail' => $urlToOrderDetail, 'intro' => $intro];
                
                foreach ($admins as $admin) {
                    $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "orderform.html.twig");
                }
                // $this->mailer->sendEmailMessage($maildata, $user->getEmail(), "Your order has been received", "order_received.html.twig");
        
                // $this->addFlash('success','Your Order has been received. We will call you soon');

                $order_id = $order->getId();
                $amountToPay = $order->getTotalAmount();
                $number = $_POST['phoneNumber'];
                $reference = "BB-ORDER-$order_id";
                $description = "Online Sale : " . $fullname . " - " . $number;
    
                // $amount = 1;
                list($status, $res) = $this->mpesa->pushSTK($amountToPay, $number, $reference, $description);
                
                return $this->redirectToRoute('checkout_confirm_payment', [
                    'amountToPay' => $amountToPay,
                    'order_id' => $order_id,
                    'status' => $status,
                    'request_id' => $res,
                    'shipping' => $shipping_cost
                ]);
    
                // pay now was clicked
                // save all required data
                // take to mpesa page
                // complete payment
                // send email to admins and client
                // go to confirmation page and show necessary info
            } elseif (isset($_POST['payOnDelivery'])) {

                $this->denyAccessUnlessGranted('ROLE_USER');
                $date = new \DateTime();
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $streetAddress = $_POST['streetAddress'];
                $shipping_cost = $_POST['shipping'];
                $cartAndShipping = $_POST['cartAndShipping'];
                $county = $_POST['county'];
                $route = $_POST['route'];
                $phoneNumber = $_POST['phoneNumber'];
                $fullname = $firstName . ' ' . $lastName;
                $user = $this->getUser();

                $existingAddress = $addressRepo->findOneByUser($user);
                
                if($existingAddress === null) {
                    $address = new Address();
                } else {
                    $address = $existingAddress;
                }
                
                $address->setUser($user);
                $address->setStreetAddress($streetAddress);
                $address->setCounty($county);
                $this->em->persist($address);
                $this->em->flush();

                $msg = "There is a new order: Name: $fullname, Street Address: $streetAddress, County: $county, Route: $route, Phone Number: $phoneNumber ";

                $cart = $cartManager->getCurrentCart($this->getUser());
                
                $cart->setOrderStatus('processing');
                // echo "<pre>";
                // var_dump($cart);
                // die();
                $cart->setTotalAmount($cartAndShipping);
                $cart->setTaxAmount(0);
                $cart->setShippingAmount($shipping_cost);
                $cart->setSubtotal($cart->getTotal());
                $cart->setShippingMethod('delivery');
                $cart->setPaymentStatus('pending');
                // $cart->setPaymentDate();
                // $cart->setNotes();
                // $cart->setPromoCode();
                $cart->setDeliveryStatus('in_transit');
                $cart->setShippingAddress($address);
                
                $message = new Message();
                $message->setName($fullname);
                $message->setEmail($user->getEmail());
                $message->setMessage($msg);
                $message->setReceivedOn($date);
                $order = $orderRepo->findOneById($cart->getId());
                $order->setUser($user);

                $this->em->persist($message);
                $this->em->persist($order);
                $this->em->flush();
                

    
                $subject = "New Unpaid Order From Website";
                $intro = "There is a new unpaid order from the website.";
                $admins = $this->userRepo->findByUsertype('admin');
                $urlToOrderDetail = $this->adminUrlGenerator->setController('App\Controller\Admin\OrderrCrudController')
                ->setAction('detail')
                ->setEntityId($cart->getId())
                ->generateUrl();
                $maildata = ['name' => $fullname, 'emailAd' => $user->getEmail(), 'subject' => $subject, 'shipping_cost' => $shipping_cost, 'cartAndShipping' => $cartAndShipping, 'message' => $msg, 'order' => $order, 'urlToOrderDetail' => $urlToOrderDetail, 'intro' => $intro];
                
                foreach ($admins as $admin) {
                    $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "orderform.html.twig");
                }
                $this->mailer->sendEmailMessage($maildata, $user->getEmail(), "Your order has been received", "order_received.html.twig");
        
                $this->addFlash('success','Your Order has been received. We will call you soon');
        
                return $this->redirectToRoute('order_received', ['order_id' => $order->getId(), 'shipping' => $shipping_cost]);
                        
                // pay on delivery was clicked
                // save all required data
                // send email to admins and client
                // got to confirmation page and show necessary info
            }
        }
    
        return $this->render('payment_page.html.twig', [
            'test'=> $request->get('test'),
        ]);
    }

}
