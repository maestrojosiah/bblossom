<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\ContactType;
use App\Repository\CategoryRepository;
use App\Repository\ProductDisplayRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Repository\WebInfoRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;

class DefaultController extends AbstractController
{

    public function __construct(private readonly SeoGeneratorProvider $seoGenerator, private readonly EntityManagerInterface $em, private readonly Mailer $mailer, private readonly WebInfoRepository $webInfoRepo){}

    #[Route('/', name: 'app_default')]
    public function index(ProductRepository $productRepo, CategoryRepository $categoryRepo, ProductDisplayRepository $productDisplayRepo, Request $request): Response
    {
        $products = $productRepo->findBy(
            ['availability' => true]
        );
        $categories = $categoryRepo->findBy(
            ['is_active' => true]
        );
        $productDisplays = $productDisplayRepo->findBy(
            ['label' => 'thin'],
            ['id'=> 'ASC']
        );
        $this->seoGenerator->get('basic')
            ->setTitle('Baby Skin Care Essentials For Eczema and Sensitive Skin')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. These products are entirely natural and have earned the trust of Kenyan families, making Baby Blossom one of the most reputable choice for both infants and mothers. Baby Blossom offers a range of 100% natural baby care products, including Calendula baby soap, baby lotion, baby wash and shampoo, and massage oil.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');
                        
        return $this->render('default/index.html.twig', [
            'products' => $products,
            'productdisplays' => $productDisplays,
            'categories' => $categories,
        ]);
    }

    #[Route('/show/profile', name: 'app_profile_show')]
    public function profile(): Response
    {
        return $this->render('default/profile.html.twig', [
            'test' => 'Test',
        ]);

    }

    #[Route('/about', name: 'app_about')]
    public function about(): Response
    {
        $about = $this->webInfoRepo->findOneByType('about');

        $this->seoGenerator->get('basic')
            ->setTitle('About Baby Blossom - All Our Ingredients')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. These products are entirely natural and have earned the trust of Kenyan families, making Baby Blossom one of the most reputable choice for both infants and mothers. Baby Blossom offers a range of 100% natural baby care products, including Calendula baby soap, baby lotion, baby wash and shampoo, and massage oil.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');

        return $this->render('default/about.html.twig', [
            'about' => $about,
        ]);

    }

    #[Route('/faq', name: 'app_faq')]
    public function faq(): Response
    {
        $faq = $this->webInfoRepo->findOneByType('faq');

        $this->seoGenerator->get('basic')
            ->setTitle('Frequently Asked Questions (FAQs)')
            ->setDescription('Baby Blossom offers a range of skincare products designed for babies with eczema or sensitive skin. These products are entirely natural and have earned the trust of Kenyan families, making Baby Blossom one of the most reputable choice for both infants and mothers. Baby Blossom offers a range of 100% natural baby care products, including Calendula baby soap, baby lotion, baby wash and shampoo, and massage oil.')
            ->setKeywords('baby lotion, baby soap, baby products, baby massage oil, best soap for newborns, infant soap, best soap for infants, soap for newborns, baby oil, infants products');

        return $this->render('default/faq.html.twig', [
            'faq' => $faq,
        ]);

    }


    public function renderFooterForm(): Response
    {
        $form = $this->createForm(ContactType::class);
        
        return $this->render('tmp/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    #[Route('/send/message/contact/form', name: 'contact_form')]
    public function contact_form(UserRepository $userRepo, Request $request): Response
    {

        // Create an instance of the form
        $form = $this->createForm(ContactType::class);
        
        // Handle form submission
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $name = $contact['name'];
            $email = $contact['email'];
            $msg = $contact['message'];
            $date = new \DateTime();

            
            $message = new Message();
            $message->setName($name);
            $message->setEmail($email);
            $message->setMessage($msg);
            $message->setReceivedOn($date);
            $this->em->persist($message);
            $this->em->flush();

            $subject = "Message From Contact Form";
            $admins = $userRepo->findByUsertype('admin');
            $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $msg];

            foreach ($admins as $admin) {
                $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
            }
            $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");
    
            $this->addFlash('success','Your Message has been sent');

        } else {

            $this->addFlash('danger','Your Message has NOT been sent');

        }
   
        return $this->redirectToRoute('app_default');

    }

    #[Route(path: '/test/test/test', name: 'multitesting')]
    public function testingmultiplethings(): Response
    {

        $arr = ["src/Controller/Admin/CategoryCrudController.php","src/Controller/DefaultController.php","src/Controller/MpesaController.php","src/Controller/ProductController.php","src/Entity/Category.php","src/Twig/GlobalVars.php","templates/tmp/header.html.twig","public/site/images/bg_with_logo.jpg","public/site/images/bg_with_logo.png","public/site/images/toys_category.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-1.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-2.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-3.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-4.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-5.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-6.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-7.jpg","public/site/images/uploads/instagram-sm/bblossom-bath-toys-8.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-1.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-2.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-3.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-4.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-5.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-6.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-7.jpg","public/site/images/uploads/instagram/Babyblossom-bath-toys-8.jpg","public/site/images/uploads/toys-category-1712678284.jpg","templates/default/category_landing.html.twig"];

        $proj_dir = $this->getParameter('proj_dir');
        $test = [];
        foreach($arr as $a) {
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }

        return $this->render('default/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }

    #[Route('/{category_slug}', name: 'category_index', requirements: ['category_slug' => '^[a-z0-9]+(-[a-z0-9]+)+$'])]
    public function index_category(ProductRepository $productRepo, CategoryRepository $categoryRepo, ProductDisplayRepository $productDisplayRepo, Request $request, $category_slug): Response
    {

        $category = $categoryRepo->findOneBySlug($category_slug);
        $products = $category->getProducts();

        $this->seoGenerator->get('basic')
            ->setTitle($category->getMetaTitle())
            ->setDescription($category->getSummary())
            ->setKeywords('baby products, baby shop');
        $this->seoGenerator->get('og')
            ->setTitle($category->getMetaTitle())
            ->setDescription($category->getSummary())
            ->setImage("https://babyblossom.co.ke/site/images/uploads/" . $category->getImageUrl())
            ->setType("website")
            ->setUrl("https://babyblossom.co.ke/".$category->getSlug());
                        
        return $this->render('default/category_landing.html.twig', [
            'products' => $products,
            'category' => $category,
        ]);
    }

}
