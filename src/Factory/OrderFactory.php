<?php

namespace App\Factory;

use App\Entity\Orderr;
use App\Entity\OrderItem;
use App\Entity\Product;

/**
 * Class OrderFactory
 * @package App\Factory
 */
class OrderFactory
{
    /**
     * Creates an Orderr.
     *
     * @return Orderr
     */
    public function create(): Orderr
    {
        $order = new Orderr();
        $order
            ->setOrderStatus(Orderr::STATUS_CART)
            ->setOrderDate(new \DateTime());
            
        return $order;
    }

    /**
     * Creates an item for a product.
     *
     * @param Product $product
     *
     * @return OrderItem
     */
    public function createItem(Product $product): OrderItem
    {
        $item = new OrderItem();
        $item->setProduct($product);
        $item->setQuantity(1);

        return $item;
    }
}